# README #

VHDL Module used to interface multiple LE58QL021FJC slic codecs with the ADSP-BF532 microprocessor using SPI bus. This module implements several glue logic and a slave SPI interface  in mode 0 (project requirement). There are some customization for the application which controls them but that can be easily changed. Module is parametrized by number of codecs which can be specified in the "generics".

### How to use it?

This code was written using Sigasi Free Edition VHDL (http://www.sigasi.com/), because quartus or vivado editors are not so... nevermind.

There are predefined SPI commands to read/write for each codec in the array and also to retrieve ID/VERSION and global IRQ status of the board. For codec R/W operations the first byte switches the SPI signals to the specific codec I/O bus, after that, commands can be sent according to codec's datasheet.  You can find predefined commands in [fxs_fxo.vhd](../master/FXS_FXO.vhd)

The main Finite State Machine controls communication between SPI master and codecs and it is totally customized for the target application.

TDM bus is bypassed, no logic is applied on these signals.

### How can I contribute? ###

- Add parametrized SPI mode operation setup in SPI_SLV_CORE module. Current version only supports mode 0.

### License ###

MIT License

2017, Alex Bohn