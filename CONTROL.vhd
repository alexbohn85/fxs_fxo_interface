-- # Module Name: SPI data control
-- # Description: Generates IRQ from SPI CS and SCLK signals
--
-- 17/09/2015 -> File Created [Alex Bohn]

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity CONTROL is
	generic(
		bus_width : integer := 8
	);
	port (
		i_clk 		: in std_logic;
		i_sclk		: in std_logic;
		i_sclk_rise : in std_logic;
		i_sclk_fall : in std_logic;
		i_rst 		: in std_logic;
		i_enable	: in std_logic;
		o_irq_spi	: out std_logic
		
	);
end entity CONTROL;

architecture RTL of CONTROL is
	
	signal COUNTER 	: integer range bus_width downto 0 := 0;
	signal w_begin		: std_logic;
	signal w_end		: std_logic;

	-----------------------------------------
	--   Finite State Machine declaration  --
	-----------------------------------------
	type t_STATES is (start, running, finish);
	signal STATE : t_STATES;
	signal NEXT_STATE : t_STATES;	
	
begin
		
	-- spi sclk counter
	proc_counter: process(i_clk)
	begin
		if (i_clk'event and i_clk = '1') then
				
			if (i_rst = '1') then
				COUNTER <= 0;
			end if;
				
			if (COUNTER = bus_width) then
				COUNTER <= 0;
			end if;
			
			if (i_sclk_fall = '1') then
				if (i_enable = '1') then
					COUNTER <= COUNTER + 1;
					if (COUNTER < bus_width ) then										
					else
						COUNTER <= 0;
					end if;
				end if;
			end if;
			
		end if;		
	end process proc_counter;
	
	
	-- first bit and last bit detectors
	w_begin <= '1' when (COUNTER = 0) else '0';
	w_end 	<= '1' when (COUNTER = bus_width) else '0';
	

	-- FSM control and refresh
	proc_FSM_control: process (i_clk, i_rst)
	begin		
		if (i_rst = '1') then
			STATE <= start;
		elsif (i_clk'event and i_clk = '1') then
			STATE <= NEXT_STATE;
		end if;			
	end process proc_FSM_control;


	-- FSM core
	proc_FSM_core: process (STATE, w_end, w_begin)
	begin	
		case (STATE) is					
			when running =>
				if (w_end = '1') then
					NEXT_STATE <= finish;
				else
					NEXT_STATE <= running;
				end if;
							
			when start =>
				NEXT_STATE <= running;
			
			when finish =>
				if (w_begin = '1') then
					NEXT_STATE <= start;
				else
					NEXT_STATE <= finish;
				end if;				
		end case;			
	end process proc_FSM_core;
	
	-- SPI IRQ 'interrupt' data was completely sent or received
	o_irq_spi <= '1' when (state = finish) else '0';

end architecture RTL;
