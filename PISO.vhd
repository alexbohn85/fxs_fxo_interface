-- # Module Name: PISO - Parallel IN Serial OUT
-- # Description: Conversor paralelo -> serial para comunicacao SPI
--
-- 17/09/2015 -> File Created [Alex Bohn]

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity PISO is
	
	generic(
		bus_width	: integer := 8
	);
	
	port (
		i_clk		: in std_logic;		
		i_sclk 		: in std_logic;
		i_sclk_rise : in std_logic;
		i_sclk_fall : in std_logic;
		i_rst 		: in std_logic;
		i_load 		: in std_logic;
		i_enable 	: in std_logic;
		i_enable_rise: in std_logic;
		i_enable_fall: in std_logic;	
		i_pi		: in std_logic_vector(bus_width-1 downto 0);
		o_so		: out std_logic
	);
	
end entity PISO;

architecture RTL of PISO is

	signal temp 	: std_logic_vector (bus_width-1 downto 0);

begin
	
	
	u0: process(i_clk)
	begin			
		
		if (i_clk'event and i_clk = '1') then			
			-- sync reset
			if (i_rst = '1') then
				temp <= (others => '0');		
			
			-- parallel input data
			elsif (i_load = '1') then
				temp <= i_pi;
							
			-- serialize it
			elsif (i_sclk_fall = '1' or i_enable_rise = '1') then			
				if (i_enable = '1') then	
					temp(7 downto 1) <= temp(6 downto 0);
					temp(0) <= '0';
				end if;				
			end if;
		end if;		
	end process u0;
	
	-- serial out
	o_so <= temp(7);
	

end architecture RTL;
