-- # Module Name: FXS_FXO modulo principal UnniTI/Impacta
-- # Description:

-- Interface de comunicacao SPI e TDM das placas de FXS e FXO para familia de PABX UnniTI e 
-- Impacta 94/140/220. O barramento de dados eh SPI modo 0 operando como slave. A maquina de estados 
-- principal recebe comandos da CPU(PABX) via SPI e eh responsavel por enviar o descritor da placa, 
-- habilitar o barramento de comunicacao com os CODECs e fornecer status de interrupcao dos codecs. 
-- O barramento TDM eh bypass para reduzir fanout do FPGA da CPU.

-- IDE: Sigasi Starter Edition
-- 23/09/2015 -> File Created [Alex Bohn]

---------------------------------------------------------------------------------
-- Comandos de comunicacao SPI FXS/FXO - PABX
-- COMANDO (hex)        DESCRICAO        
--   01					Identidade da Placa
--   02					Status de interrupcao dos codecs
--   03 				Numero de bytes do Identificador.
--   10					Escrita em  Codec 1 (ramais de 1 a 4)
--   20                 Escrita em  Codec 2 (ramais de 5 a 8)
--   40					Escrita em  Codec 3 (ramais de 9 a 12)
--   80                 Escrita em  Codec 4 (ramais de 13 a 16)
--   A0					Escrita em  Codec 5 (ramais de 17 a 20)
--   C0					Escrita em  Codec 6 (ramais de 21 a 24)
--   11 				Leitura do  Codec 1 (ramais de 1 a 4)
--   21 				Leitura do  Codec 2 (ramais de 5 a 8)
--   41  	 			Leitura do  Codec 3 (ramais de 9 a 12)
--   81 				Leitura do  Codec 4 (ramais de 12 a 16)
--   A1 				Leitura do  Codec 5 (ramais de 17 a 20)
--   C1 				Leitura do  Codec 6 (ramais de 21 a 24)
---------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- definicoes globais
use work.FXS_FXO_GLOBALS.all;

entity FXS_FXO is	
	generic (
		BOARD : type_info;
		DESCRITOR: type_descritor
	);
	
	port(
		
		i_CLOCK	 		: in    std_logic;
		i_RST 	 		: in    std_logic;											 	
		
		-- Status LED / Debug
		o_LED_STATUS 	: out 	std_logic;		
		
		-- SPI port
		i_SCLK     		: in    std_logic;
		i_MOSI      	: in    std_logic;
		i_CS        	: in    std_logic;
		o_MISO      	: out   std_logic;
		o_INTPLACA  	: out   std_logic;
		
		-- TDM interface
		i_DR    		: in    std_logic;
		o_DR    		: out   std_logic;
		i_DX    		: in    std_logic;
		o_DX    		: out   std_logic;
		i_F0   			: in    std_logic;
		o_F0   			: out   std_logic;
		i_C2			: in    std_logic;
		o_C2			: out   std_logic;
		
		-- CODEC interface
		o_CSCODEC		: out   std_logic_vector(BOARD.num_codecs-1 downto 0);
		i_INT_CODEC 	: in    std_logic_vector(BOARD.num_codecs-1 downto 0);		
		io_CODEC_DIO	: inout std_logic;
		o_SCLK_CODEC	: out 	std_logic_vector(BOARD.num_codecs-1 downto 0)
		);
		
end entity FXS_FXO;

architecture RTL of FXS_FXO is
		
		
	-- Maquina de Estados: (comunicacao SPI) CPU <-> CPLD	
	signal STATE 	: type_main_fsm := INIT;		
	
	-- SPI_SLV_CORE 
	signal IRQ		: std_logic; -- interrupcao para evento de dado enviado/recebido	
	signal LOAD		: std_logic; -- carrega dado no buffer paralelo para envio
	signal S_IN 	: std_logic; -- serial data in
	signal S_OUT 	: std_logic; -- serial data out	
	signal P_IN  	: std_logic_vector(BUS_WIDTH-1 downto 0); -- parallel data in
	signal P_OUT  	: std_logic_vector(BUS_WIDTH-1 downto 0); -- parallel data out	
	
	-- Sinais internos do SPI
	signal MOSI 			: std_logic;	-- sinal MOSI interno
	signal MISO 			: std_logic;    -- sinal MISO interno
	signal MISO_SPI_CORE 	: std_logic;	-- derivacao do sinal MISO para modulo SPI_SLV_CORE
	signal ENABLE 			: std_logic;	-- sinal interno do chipselect
	signal w_SCLK			: std_logic;	-- sclk wire
	
	-- CODEC > controle da porta DIO
	signal CODEC_DIO_DIR 	: std_logic := READD;
	signal CODEC_DIO_ENA 	: std_logic := DISABLED; -- a leitura do reg de status de interrupcao soh eh liberado apos a leitura do descritor
	signal CODEC_DIO_BUF 	: std_logic;
	
	-- CODEC > chip select buffer
	signal CODEC_CS_CODE 	: std_logic_vector(3 downto 0); -- dado recebido do chipselect
	signal CODEC_CS_BUF		: std_logic_vector(BUS_WIDTH-1 downto 0) := x"FF";	-- buffer chipselect
	
	-- CODEC > interruption status buffer
	signal CODEC_INT_BUF 	: std_logic_vector(BUS_WIDTH-1 downto 0) := x"00";	
	signal CODEC_INT_ENA 	: std_logic := DISABLED;  -- habilita entrada de interrupcoes dos 
													  -- codecs apos termino de envio do descritor.
	-- LED CONTROL	
	signal LED_DRIVER 	   : std_logic;
	constant FREQ_CLOCK    : integer := 8000; 	-- clock F0 8khz
	signal LED_CADENCE 	   : type_led_ctrl;
	signal COUNT_ON        : integer range 0 to 3 * FREQ_CLOCK := 0;
	signal COUNT_OFF       : integer range 0 to 3 * FREQ_CLOCK := 0;
	signal led_count	   : natural range 0 to 3 * FREQ_CLOCK := 0;
	signal clk_led         : std_logic;
	
	-- sinal de RESET interno para testes no KIT MAV V - ativo baixo!
	signal i_RESET			: std_logic;		
	
	-- Controle do pino bi-direcional (Data IO dos Codecs)
	signal in_bidir : std_logic;
	signal out_bidir : std_logic;
	
	-- sync reset signals
	signal reset_r : std_logic;
	signal reset_s : std_logic;
	signal reset_t : std_logic;
	
	signal w_F0 : std_logic;
	signal F0_r : std_logic;
	signal F0_s : std_logic;
	signal F0_t : std_logic;
	
	
---------------------------------
--           BEGIN           ----
---------------------------------	
begin	

	-- SPI Slave instance
	spi_ins : entity work.SPI_SLV_CORE
		generic map(
			bus_width => BUS_WIDTH
		)
		port map(
			i_clk     => i_CLOCK,
			i_rst     => i_RESET,
			i_sclk    => i_SCLK,
			i_enable  => ENABLE,
			i_load    => LOAD,
			o_irq_spi => IRQ,
			i_si      => S_IN,
			o_so      => S_OUT,
			i_pi      => P_IN,
			o_po      => P_OUT
		);
	
	
---------------------------------
--     Generic glue logic      --
---------------------------------
	
	-- TDM codec ->> pabx
	o_DR <= i_DR;
	
	-- TDM codec <<- pabx
	o_DX <= i_DX;
	
	-- TDM clock
	o_C2 <= i_C2;
	
	-- TDM frame sync
 -- o_F0 <= i_F0;
 	o_F0 <= w_F0;
	
	
	-- clock para LED
 --	clk_led <= i_F0; -- usando sinal F0 de 8kHz para controle do LED (menos recursos)
 	clk_led <= w_f0;

	-- DFF amostra sinais de entrada/saida do bi-direcional
	process(i_CLOCK)
	begin
		if (i_CLOCK = '1' and i_CLOCK'event) then
			in_bidir <= MOSI;
			CODEC_DIO_BUF <= out_bidir;
		end if;
	end process;
	
	-- controle do pino bi-direcional
	process (CODEC_DIO_ENA, CODEC_DIO_DIR, in_bidir, io_CODEC_DIO) 
        begin
        if( CODEC_DIO_ENA = ENABLED and CODEC_DIO_DIR = READD) then
            io_CODEC_DIO <= 'Z';
            out_bidir <= io_CODEC_DIO;
        else
            io_CODEC_DIO <= in_bidir; 
            out_bidir <= io_CODEC_DIO;
        end if;
    end process;
		

	-- Registrador de IRQ dos codecs (codec ativo baixo /INT)
	-- OBS: O dado lido pela CPU(PABX) eh ativo alto, por isso o 'not' no registrador.
	CODEC_INT_BUF(BOARD.num_codecs-1 downto 0) <= not i_INT_CODEC;
	
	-- Logica que gera interrupcao para CPU(PABX)
	o_INTPLACA <= '0' when (CODEC_INT_BUF /= x"00" and CODEC_INT_ENA = ENABLED) else '1';	
	
	-- CS auxiliar para uso como enable interno
	ENABLE <= not i_CS;
		
	-- CODEC controle da porta DIO
	CODEC_DIO_ENA <= DISABLED when (CODEC_CS_BUF = x"FF") else ENABLED;
		
	-- MISO Mux 2x1
	MISO <= CODEC_DIO_BUF when (CODEC_DIO_ENA = ENABLED and CODEC_DIO_DIR = READD) else MISO_SPI_CORE;	
	
	-- MISO - saida tristate
	o_MISO <= MISO when ENABLE = ENABLED else 'Z';
	
	-- MOSI - entrada
	MOSI <= i_MOSI;
	
	-- CS_CODEC
	o_CSCODEC <= CODEC_CS_BUF(BOARD.num_codecs-1 downto 0);
	
	-- SPI_SLV_CORE MOSI buffer
	S_IN <= MOSI;
	
	MISO_SPI_CORE <= S_OUT;	
		
	-- SCLK wire
	w_SCLK <= i_SCLK;	
	
	-- reset assincrono
	--i_RESET <= not i_RST;
	
	-- reset sincrono
	reset: process (i_CLOCK)
	begin
		if(i_CLOCK'event and i_CLOCK = '1') then
			reset_r <= not i_RST;
			reset_s <= reset_r;
			reset_t <= reset_s;
		end if;
	end process;
	
	i_RESET <= '1' when reset_s = '1' and reset_t = '1' else '0';
	
	
	f0: process (i_CLOCK)
	begin
		if(i_CLOCK'event and i_CLOCK = '0') then
			F0_r <= i_F0;
			F0_s <= F0_r;
		--	F0_t <= F0_s;
		end if;
	end process;
	
	w_F0 <= '1' when F0_r = '1' and F0_s = '1' else '0';
	--w_F0 <= F0_r and i_CLOCK;
	

-------------------------------------------------------
--       Maquina de Estados - Comunicacao SPI        --
-------------------------------------------------------
	
	fsm: process(i_CLOCK)
		variable cnt : integer range 0 to DESCRITOR'length;
	begin		
		if (i_CLOCK'event and i_CLOCK = '1') then
			if (i_RESET = '1') then
				STATE   <= INIT;				
				cnt := 0;
				LED_CADENCE <= LED_50ms;
			else
				if (IRQ = '1') then
					case STATE is				
	
--  <------------------- INIT ----------------------->		
					when INIT =>		
						-- comando 01h - envia descritor					
						if ( P_OUT = CMD_SEND_ID ) then							
							LOAD <= ENABLED;							
							P_IN <= DESCRITOR(0); -- envia primeiro byte do array
							cnt := cnt + 1;
							STATE <= ID_BUSY;
						end if;
						
						-- comando 02h - envia status de interrupcoes
						if ( P_OUT = CMD_SEND_IRQ_STATUS ) then
							LOAD <= ENABLED;
							P_IN <= CODEC_INT_BUF;							
						end if;
										
						-- comando 03h - envia tamanho do descritor					
						if ( P_OUT = CMD_SEND_ID_SIZE ) then
							LOAD <= ENABLED;
							P_IN <= std_logic_vector(to_unsigned(DESCRITOR'length, 8));
						end if;
										
						-- comandos leitura/escrita
						if ( P_OUT(7 downto 4) >= x"1") then							
							if (P_OUT(0) = READD) then
								CODEC_DIO_DIR <= READD;
								CODEC_CS_CODE <= P_OUT(7 downto 4); -- chip select codec			
								STATE <= CODEC_BUSY;								
							end if;
								
							if (P_OUT(0) = WRITEE) then
								CODEC_DIO_DIR <= WRITEE;
								CODEC_CS_CODE <= P_OUT(7 downto 4);	-- chip select codec			
								STATE <= CODEC_BUSY;								
							end if;
														
						end if;
	
--  <--------------------CODEC_BUSY---------------------->
					-- comunicacao CPU <-> CODEC (1 byte)
					when CODEC_BUSY =>
						-- desabilita chip select do codec pois ja houve o envio/recepcao									
						CODEC_CS_CODE <= SELECT_IDLE;
						STATE <= INIT;
						
--  <--------------------ID_BUSY--------------------->
					-- enviando descritor para a CPU
					when ID_BUSY =>					
						if (cnt < DESCRITOR'length) then
							LOAD <= ENABLED;
							P_IN <= DESCRITOR(cnt);
							cnt := cnt + 1;
						else
							CODEC_INT_ENA <= ENABLED; -- descritor ok! habilitar recebimento de interrupcoes dos codecs							
							LED_CADENCE <= LED_500ms; -- altera cadencia do led indicando o fim da leitura do descritor
							STATE <= INIT;
							cnt := 0;
						end if;
					end case;
					
				else
					LOAD <= DISABLED; -- libera LOAD ou o dado nunca sera enviado!
				end if;
				
			end if;
		end if;
	end process;
	
-------------------------------------
--          Generates              --
-------------------------------------
	
	gen_led_status:
	if BOARD.led = LED_ENABLED generate	
		
		-- pisca pisca da alegria =]
		led_proc : process (clk_led, i_RESET)				
		begin				
			if (i_RESET = '1') then
				led_count <= 0;
				LED_DRIVER <= '1';				
										
			elsif(clk_led'event and clk_led = '1') then	
				if (LED_DRIVER = '1') then						
					led_count <= led_count + 1;						
					if(led_count = COUNT_ON) then
						led_count <= 0;
						LED_DRIVER <= '0';
					end if;						
				else
					led_count <= led_count + 1;						
					if(led_count = COUNT_OFF) then
						led_count <= 0;
						LED_DRIVER <= '1';
					end if;													
				end if;
									
			end if;
		end process led_proc;
		
		-- led output
		o_LED_STATUS <= LED_DRIVER;
		
		-- selecao do periodo de cadencia do led
		led_ctrl_proc: process(LED_CADENCE)
		begin			
			case LED_CADENCE IS
			when LED_50ms =>
				COUNT_ON <= FREQ_CLOCK / 20;
				COUNT_OFF <= FREQ_CLOCK / 20;
			when LED_100ms =>
				COUNT_ON <= FREQ_CLOCK / 10;
				COUNT_OFF <= FREQ_CLOCK / 10;								
			when LED_500ms =>
				COUNT_ON <= FREQ_CLOCK / 2;
				COUNT_OFF <= FREQ_CLOCK / 2;
			when LED_1s =>
				COUNT_ON <= FREQ_CLOCK;
				COUNT_OFF <= FREQ_CLOCK;
			when others =>
				COUNT_ON <= FREQ_CLOCK / 1;
				COUNT_OFF <= FREQ_CLOCK / 3;
			end case;								
		end process led_ctrl_proc;		
	end generate;	

	
	-- Sinais de SCLK sao replicados (cada codec recebe um o sclk por um pino)
	-- TODO verificar se um pino apenas ja "resolve"
	gen_sclk_codec: for I in 0 to (BOARD.num_codecs-1) generate
			o_SCLK_CODEC(I) <= w_SCLK;
	end generate;

	
	-- FXS unniti
	gen0:	
	if BOARD.nome = hw_fxs_unniti generate
			-- chip select CODECs
			CODEC_CS_BUF(0) <= i_CS when CODEC_CS_CODE = SELECT_CD1 else '1';
			CODEC_CS_BUF(1) <= i_CS when CODEC_CS_CODE = SELECT_CD2 else '1';
			CODEC_CS_BUF(2) <= i_CS when CODEC_CS_CODE = SELECT_CD3 else '1';
			CODEC_CS_BUF(3) <= i_CS when CODEC_CS_CODE = SELECT_CD4 else '1';					
		end generate;
		
	-- FXO unniti
	gen1:
	if BOARD.nome = hw_fxo_unniti generate		
			-- chip select CODECs
			CODEC_CS_BUF(0) <= i_CS when CODEC_CS_CODE = SELECT_CD1 else '1';
			CODEC_CS_BUF(1) <= i_CS when CODEC_CS_CODE = SELECT_CD2 else '1';			
		end generate;		

	-- FXS 24 ramais impacta
	gen3:
	if BOARD.nome = hw_fxs24_impacta generate
			-- chip select CODECs
			CODEC_CS_BUF(0) <= i_CS when CODEC_CS_CODE = SELECT_CD1 else '1';
			CODEC_CS_BUF(1) <= i_CS when CODEC_CS_CODE = SELECT_CD2 else '1';
			CODEC_CS_BUF(2) <= i_CS when CODEC_CS_CODE = SELECT_CD3 else '1';
			CODEC_CS_BUF(3) <= i_CS when CODEC_CS_CODE = SELECT_CD4 else '1';
			CODEC_CS_BUF(4) <= i_CS when CODEC_CS_CODE = SELECT_CD5 else '1';
			CODEC_CS_BUF(5) <= i_CS when CODEC_CS_CODE = SELECT_CD6 else '1';		
	end generate;
	

end architecture RTL;
