-- # Package Name: FXS_FXO_GLOBALS
-- # Description: Package para interface FXS/FXO familia UNNITI e IMPACTA

-- 03/10/2015 -> File Created [Alex Bohn]

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- definicoes globais do projeto
package FXS_FXO_GLOBALS is

		
	constant READD     : std_logic := '1';
	constant WRITEE    : std_logic := '0';
	constant ENABLED   : std_logic := '1';
	constant DISABLED  : std_logic := '0';	
	constant BUS_WIDTH : integer   := 8;
	
	----------------------------------------
	-- Comandos SPI                       --
	----------------------------------------	
	constant CMD_SEND_ID : std_logic_vector := x"01";
	constant CMD_SEND_ID_SIZE : std_logic_vector := x"03";
	constant CMD_SEND_IRQ_STATUS : std_logic_vector := x"02";
	
	----------------------------------------
	-- Chip Selects                       --
	----------------------------------------
	constant SELECT_CD1 : std_logic := '1';
	constant SELECT_CD2 : std_logic := '2';
	constant SELECT_CD3 : std_logic := '4';
	constant SELECT_CD4 : std_logic := '8';
	constant SELECT_CD5 : std_logic := 'A';
	constant SELECT_CD6 : std_logic := 'C';
	constant SELECT_IDLE: std_logic := 'F'; -- desabilita o chipselect
	
	----------------------------------------
	-- Tipos                              --
	----------------------------------------		
	type type_led is (
		LED_ENABLED, 
		LED_DISABLED
		);
	
	type type_led_ctrl is (
		LED_50ms, 
		LED_100ms, 
		LED_500ms, 
		LED_1s
		);
	
	type type_main_fsm is ( 
		INIT, 
		CODEC_BUSY, 
		ID_BUSY	
		); -- Maquina de Estados - Protocolo de comunicacao
	
	type type_nomes is ( 
		hw_fxs_unniti,	
		hw_fxo_unniti, 
		hw_fxs_impacta, 
		hw_fxs24_impacta, 
		hw_2x12_impacta, 
		hw_fxo_impacta
		); -- Nome da placa (rotulo)
		
	type type_descritor is array (integer range <>) of std_logic_vector(7 downto 0); -- array para criar os descritores	
	
	type type_info is 
		record	
			num_codecs 	: natural;
			led			: type_led;
			nome		: type_nomes;
	end record; -- container com informacoes
	
	
	----------------------------------------
	-- Descritores e informacoes da placa --
	----------------------------------------

	-- RAMAL UNNITI	
	constant descritor_fxs_unniti: type_descritor(0 to 32) := 
	(	x"0B", x"03", x"20", -- desc header
		x"00", x"00", x"1A", x"1A", x"1A", x"1A", x"FF", -- desc codec 1
		x"00", x"01", x"1A", x"1A", x"1A", x"1A", x"FF", -- desc codec 2
		x"00", x"02", x"1A", x"1A", x"1A", x"1A", x"FF", -- desc codec 3
		x"00", x"03", x"1A", x"1A", x"1A", x"1A", x"FF", -- desc codec 4
		x"FF", x"FF" -- desc end
	);	
	constant info_fxs_unniti: type_info := (4, LED_ENABLED, hw_fxs_unniti);
	
	
	-- TRONCO UNNITI	
	--	constant descritor_fxo_unniti: type_descritor(0 to 19) := 
	--	(	x"0C", x"03", x"20", x"0F", -- desc header
	--		x"00", x"00", x"2F", x"2F", x"2F", x"2F", x"2F", -- desc codec 1
	--		x"00", x"01", x"2F", x"2F", x"2F", x"2F", x"2F", -- desc codec 2		
	--		x"FF", x"FF" -- desc end
	--	);
	
	-- TRONCO IMPACTA
	constant descritor_fxo_unniti: type_descritor(0 to 19) := 
	(	x"0C", x"03", x"20", x"0F", -- desc header
		x"00", x"00", x"31", x"31", x"31", x"31", x"FF", -- desc codec 1
		x"00", x"01", x"31", x"31", x"31", x"31", x"FF", -- desc codec 2		
		x"FF", x"FF" -- desc end
	);
	constant info_fxo_unniti: type_info := (2, LED_ENABLED, hw_fxo_unniti);
	
	
--	-- 24x RAMAL IMPACTA	
--	constant descritor_fxs24_impacta: type_descritor(0 to 46) := 
--	(	x"0B", x"03", x"20", -- desc header
--		x"00", x"00", x"1A", x"1A", x"1A", x"1A", x"FF", -- desc codec 1
--		x"00", x"01", x"1A", x"1A", x"1A", x"1A", x"FF", -- desc codec 2
--		x"00", x"02", x"1A", x"1A", x"1A", x"1A", x"FF", -- desc codec 3
--		x"00", x"03", x"1A", x"1A", x"1A", x"1A", x"FF", -- desc codec 4
--		x"00", x"03", x"1A", x"1A", x"1A", x"1A", x"FF", -- desc codec 5
--		x"00", x"03", x"1A", x"1A", x"1A", x"1A", x"FF", -- desc codec 6
--		x"FF", x"FF" -- desc end
--	);	
--	constant info_fxs24_impacta: type_info := (6, LED_ENABLED, hw_fxs24_impacta);

end package FXS_FXO_GLOBALS;




