Editor VHDL utilizado: Sigasi Studio

1) Para criar o projeto no Sigasi Studio

 a. Para importar o projeto clique com botao direito no painel "Project Explorer" clique em "Import" 
 b. Selecione a opcao "General > Existing Projects into Workspace" 
 c. Selecione "Select root directory" e clique em Browser, aponte para o diretorio '../src' onde
    se encontram os arquivos fonte do projeto. 
 d. Clique em "Finish".

2) Para compilar utilize o Quartus 13.0.0, abra o projeto da placa desejada.

OBS: - Aqui se encontram todos os arquivos fonte para as interfaces FXS e FXO.
     - Cada placa possui em arquivo "TOP_XXX" que define a configura��o do hardware.
     - No arquivo "FXS_FXO_GLOBALS.vhd" sao declarados os DESCRITORES das placas entre outros
     tipos, dados e constantes.

---------------------------------------------------------------------------------------------     

////// Descri��o dos m�dulos ////////

PISO - serializa o dado de entrada em funcao do sinal de clock de entrada do SPI.
SIPO - deserializa a entrada de dado serial MOSI e armazena no registrador.
CONTROL - fornece um sinal de controle para saber quando um dado foi totalmente 
			lido ou enviado pelo SPI.
SYNC_SAMPLER - bloco para sincronizar os sinais externos com o clock interno.
SPI_SLV_CORE - wrapper que forma o modulo de SPI slave (PISO, SIPO, CONTROL, SYNC_SAMPERs)
FXS_FXO - componente principal e generico que controla a interface FXS ou FXO 
(Comunicacao SPI CPU, Codecs, Controle do led, Descritores)
---------------------------------------------------------------------------------------------

//////   Arquivos TOP   ///////

Para cada tipo de placa existe um TOP diferente que configura os pinos e quantidade 
de codecs da placa.
