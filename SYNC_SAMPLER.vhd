--# Description: sincronizador

-- 10/12/2015 -> File Created [Alex Bohn]

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity SYNC_SAMPLER is
	port (
		clk : in std_logic;
		rst : in std_logic;
		din : in std_logic;
		dout : out std_logic;
		d_rise: out std_logic;
		d_fall: out std_logic
		
	);
end entity SYNC_SAMPLER;

architecture RTL of SYNC_SAMPLER is
	signal din_R : std_logic;
	signal din_S : std_logic;
	signal din_T : std_logic;
	signal din_fall : std_logic;
	signal din_rise : std_logic;	
	
begin
	
	u1 : process(clk)		
	begin
		if (clk'event and clk = '1') then
			if (rst = '1') then
				din_R <= '0';
				din_S <= '0';
				din_T <= '0';
			else
				din_R <= din;
				din_S <= din_R;
				din_T <= din_S;
			end if;
		end if;
	end process u1;
 	
	u2 : process(clk)														
	begin
		if (clk'event and clk = '0') then
			din_fall <= (not (din_R)) and (not (din_S)) and din_T;
			din_rise  <= din_R and din_S and (not (din_T));
		end if;
	end process u2;
	
	d_rise <= din_rise;
	d_fall <= din_fall;

	u3 : process(clk)
	begin
		if (clk'event and clk = '0') then
			if (rst = '1') then
				dout <= '0';
			else
				if (din_fall = '1') then
					dout <= '0';
				end if;
				if (din_rise = '1') then
					dout <= '1';
				end if;
			end if;
		end if;
	end process u3;

end architecture RTL;
