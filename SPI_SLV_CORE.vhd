-- # Module Name: SPI Slave Core
-- # Description: SPI modulo escravo (SPI Mode 0)
--
-- 17/09/2015 -> File Created [Alex Bohn]

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity SPI_SLV_CORE is
	generic(
		bus_width : integer := 8
	);

	port(

		-- system clock and reset
		i_clk     : in  std_logic;
		i_rst     : in  std_logic;

		-- SPI clock
		i_sclk    : in  std_logic; 

		-- control port
		i_enable  : in  std_logic;      -- SPI CS - Chip Select
		i_load    : in  std_logic;      -- load = 1 to push data into the "tx buffer"
		o_irq_spi : out std_logic;      -- IRQ when data was completely sent or received

		-- serial IO port (SPI miso/mosi ports)
		i_si      : in  std_logic;      -- serial data IN
		o_so      : out std_logic;      -- serial data OUT

		-- parallel IO port
		i_pi      : in  std_logic_vector(BUS_WIDTH - 1 downto 0); -- parallel data IN
		o_po      : out std_logic_vector(BUS_WIDTH - 1 downto 0) -- parallel data OUT		

	);

end entity SPI_SLV_CORE;

architecture RTL of SPI_SLV_CORE is

	--TODO testar o modulo sem uso dos sincronizadores

	signal sync_sclk        : std_logic;
	signal sync_sclk_rise   : std_logic;
	signal sync_sclk_fall   : std_logic;
	signal sync_enable      : std_logic;
	signal sync_enable_rise : std_logic;
	signal sync_enable_fall : std_logic;
	signal sync_si          : std_logic;
	

begin

	------------------------------
	--:: component instances
	------------------------------
		
	u0 : entity work.CONTROL
		generic map(
			bus_width => bus_width
		)
		port map(
			i_clk       => i_clk,
			i_sclk      => sync_sclk,
			i_sclk_rise => sync_sclk_rise,
			i_sclk_fall => sync_sclk_fall,
			i_rst       => i_rst,
			i_enable    => sync_enable,
			o_irq_spi   => o_irq_spi
		);

	
	u1 : entity work.PISO
		generic map(
			bus_width => bus_width
		)
		port map(
			i_clk         => i_clk,
			i_sclk        => sync_sclk,
			i_sclk_rise   => sync_sclk_rise,
			i_sclk_fall   => sync_sclk_fall,
			i_rst         => i_rst,
			i_load        => i_load,
			i_enable      => sync_enable,
			i_enable_rise => sync_enable_rise,
			i_enable_fall => sync_enable_fall,
			i_pi          => i_pi,
			o_so          => o_so
		);


	u2 : entity work.SIPO
		generic map(
			bus_width => bus_width
		)
		port map(
			i_clk         => i_clk,
			i_sclk        => sync_sclk,
			i_sclk_rise   => sync_sclk_rise,
			i_sclk_fall   => sync_sclk_fall,
			i_rst         => i_rst,
			i_enable      => sync_enable,
			i_enable_rise => sync_enable_rise,
			i_enable_fall => sync_enable_fall,
			i_si          => sync_si,
			o_po          => o_po
		);

	-- SYNC SCLK signal		
	u3 : entity work.SYNC_SAMPLER
		port map(
			clk    => i_clk,
			rst    => i_rst,
			din    => i_sclk,
			dout   => sync_sclk,
			d_rise => sync_sclk_rise,
			d_fall => sync_sclk_fall
		);

	-- SYNC (CS) CHIP SELECT signal
	u4 : entity work.SYNC_SAMPLER
		port map(
			clk    => i_clk,
			rst    => i_rst,
			din    => i_enable,
			dout   => sync_enable,
			d_rise => sync_enable_rise,
			d_fall => sync_enable_fall
		);

	-- SYNC MOSI signal
	u5 : entity work.SYNC_SAMPLER
		port map(
			clk    => i_clk,
			rst    => i_rst,
			din    => i_si,
			dout   => sync_si
		);

end architecture RTL;
