-- # Module Name: SIPO - Serial IN Parallel OUT
-- # Description: Conversor serial -> paralelo para comunicacao SPI
--
-- 17/09/2015 -> File Created [Alex Bohn]


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity SIPO is
	generic(
		bus_width : integer := 8
	);
	
	port (
		i_clk		: in std_logic;
		i_sclk 		: in std_logic;
		i_sclk_rise : in std_logic;
		i_sclk_fall : in std_logic;
		i_rst 		: in std_logic;
		i_enable 	: in std_logic;
		i_enable_rise : in std_logic;
		i_enable_fall : in std_logic;
		i_si 		: in std_logic;
		o_po 		: out std_logic_vector(bus_width-1 downto 0)
	);
end entity SIPO;

architecture RTL of SIPO is	
	
	signal temp : std_logic_vector(bus_width-1 downto 0) := (others => '0');		

begin
	
	process(i_clk)
	begin
		if(i_clk'event and i_clk = '1') then			
			if (i_rst = '1') then
				temp <= (others => '0');			
			elsif (i_sclk_rise = '1' and i_enable = '1') then									
					temp(bus_width-1 downto 0) <= temp(bus_width-2 downto 0) & i_si;				
			end if;
		end if;	
		
	end process;	
	o_po <= temp;
	
end architecture RTL;
