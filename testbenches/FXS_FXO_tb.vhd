library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.FXS_FXO_GLOBALS.all;

entity FXS_FXO_tb is
	generic(
		BOARD		: type_info 		:= info_fxs_unniti;
		DESCRITOR	: type_descritor 	:= descritor_fxs_unniti	
	);
end entity FXS_FXO_tb;

architecture RTL of FXS_FXO_tb is
component FXS_FXO
	generic(BOARD     : type_info;
		    DESCRITOR : type_descritor);
	port(i_CLOCK      : in    std_logic;
		 i_RESET      : in    std_logic;
		 o_LED_STATUS : out   std_logic;
		 i_SCLK       : in    std_logic;
		 i_MOSI       : in    std_logic;
		 i_CS         : in    std_logic;
		 o_MISO       : out   std_logic;
		 o_INTPLACA   : out   std_logic;
		 --i_DR         : in    std_logic;
		 --o_DR         : out   std_logic;
		 --i_DX         : in    std_logic;
		 --o_DX         : out   std_logic;
		 --i_F0         : in    std_logic;
		 --o_F0         : out   std_logic;
		 --i_C2         : in    std_logic;
		 --o_C2         : out   std_logic;
		 o_CSCODEC    : out   std_logic_vector(BOARD.num_codecs - 1 downto 0);
		 i_INT_CODEC  : in    std_logic_vector(BOARD.num_codecs - 1 downto 0);
		 io_CODEC_DIO : inout std_logic);
end component FXS_FXO;
	
	
	signal t_clk        : std_logic := '0';
	signal t_sclk       : std_logic := '0';
	signal t_rst        : std_logic := '1';
	signal t_enable     : std_logic := '0';
	signal t_si         : std_logic := '0';
	signal t_so         : std_logic := '0';
	signal t_intplaca	: std_logic;
	signal t_cscodec	: std_logic_vector(BOARD.num_codecs-1 downto 0);
	signal t_intcodec	: std_logic_vector(BOARD.num_codecs-1 downto 0);
	signal t_led		: std_logic := '0';
	signal t_dio		: std_logic := '0';
	
	
begin
	
	-- system clock generator
	t_clk <= not t_clk after 50 ns;
	
	-- SPI clock generator
	t_sclk <= not t_sclk after 10 us when t_enable = '1' else '0';
	
	-- chip select disable control
	t_enable <= '1';
	
	-- int codec -> idle = '1'
	
	-- system reset generator
	reset_proc : process
	begin
		t_rst <= '1';
		wait for 50 ns;
		t_rst <= '0';
		wait;
	end process reset_proc;
	
	FXS_FXO_inst : component FXS_FXO
		generic map(
			BOARD     => BOARD,
			DESCRITOR => DESCRITOR
		)
		port map(
			i_CLOCK      => t_clk,
			i_RESET      => t_rst,
			o_LED_STATUS => t_led,
			i_SCLK       => t_sclk,
			i_MOSI       => t_si,
			i_CS         => t_enable,
			o_MISO       => t_so,
			o_INTPLACA   => t_intplaca,
			--i_DR         => i_DR,
			--o_DR         => o_DR,
			--i_DX         => i_DX,
			--o_DX         => o_DX,
			--i_F0         => i_F0,
			--o_F0         => o_F0,
			--i_C2         => i_C2,
			--o_C2         => o_C2,
			o_CSCODEC    => t_cscodec,
			i_INT_CODEC  => t_intcodec,
			io_CODEC_DIO => t_dio
		);
	
	

end architecture RTL;
