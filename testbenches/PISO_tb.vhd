library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity tb_PISO is
	generic (
		bus_width : integer := 8
	);
end entity tb_PISO;

architecture RTL of tb_PISO is
	
	component PISO
		generic(
			bus_width : integer := 8
		);		
		port(
			i_sclk		: in  std_logic;
			i_rst 		: in  std_logic;
			i_load  	: in  std_logic;
			i_enable 	: in  std_logic;
			i_pi     	: in  std_logic_vector(bus_width - 1 downto 0);
			o_so     	: out std_logic
		);		
	end component PISO;
	
	signal t_clk 	: std_logic;
	signal t_rst 	: std_logic;
	signal t_enable : std_logic;
	signal t_load 	: std_logic;
	signal t_pi 	: std_logic_vector(7 downto 0);
	signal o_so 	: std_logic;
	
begin
	
	clk_proc: process
	begin
		t_clk <= '0';
		wait for 10 ns;
		t_clk <= '1';
		wait for 10 ns;
	end process clk_proc;
	
	reset_proc: process
	begin
		t_rst <= '1';
		wait for 25 ns;
		t_rst <= '0';
		wait;
	end process reset_proc;
	
	load_proc: process
	begin
		-- 1st byte
		t_load <= '0';
		wait for 60 ns;
		t_load <= '1';
		wait for 30 ns;
		t_load <= '0';
		
		-- 2nd byte
		wait for 240 ns;
		t_load <= '1';
		wait for 40 ns;
		t_load <= '0';
		wait;
		
	end process load_proc;
	
	enable_proc: process
	begin
		t_enable <= '0';
		wait for 60 ns;
		t_enable <= '1';
		wait;
	end process enable_proc;
	
	dataIN_proc: process
	begin
		t_pi <= "00000001";
		wait for 230 ns;
		t_pi <= "00000001";
		wait;
	end process dataIN_proc;
	
	PISO_inst : component PISO
		generic map(
			bus_width => bus_width
		)
		port map(		   
			i_sclk   => t_clk,
			i_rst    => t_rst,
			i_load   => t_load,
			i_enable => t_enable,
			i_pi     => t_pi,
			o_so     => o_so
		);		

end architecture RTL;
