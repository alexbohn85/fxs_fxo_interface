library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.FXS_FXO_GLOBALS.all;

entity TOP_RAMAL_tb is
	generic(
		BOARD     : type_info      := info_fxs_unniti;
		DESCRITOR : type_descritor := descritor_fxs_unniti
	);
end entity TOP_RAMAL_tb;

architecture RTL of TOP_RAMAL_tb is
	component TOP_RAMAL_UNNITI
		generic(BOARD     : type_info      := info_fxs_unniti;
			    DESCRITOR : type_descritor := descritor_fxs_unniti);
		port(o_RS_PROG_B_n : out   std_logic;
			 o_TESTE       : out   std_logic_vector(2 downto 0);
			 i_CLOCK       : in    std_logic;
			 i_RESET       : in    std_logic;
			 o_LED_STATUS  : out   std_logic;
			 i_SCLK        : in    std_logic;
			 i_MOSI        : in    std_logic;
			 i_CS          : in    std_logic;
			 o_MISO        : out   std_logic;
			 o_INTPLACA    : out   std_logic;
			 o_CSCODEC     : out   std_logic_vector(BOARD.num_codecs - 1 downto 0);
			 i_INT_CODEC   : in    std_logic_vector(BOARD.num_codecs - 1 downto 0);
			 io_CODEC_DIO  : inout std_logic);
	end component TOP_RAMAL_UNNITI;
	
	component SYNC_SAMPLER
		port(clk    : in  std_logic;
			 rst    : in  std_logic;
			 din    : in  std_logic;
			 dout   : out std_logic;
			 d_rise : out std_logic;
			 d_fall : out std_logic);
	end component SYNC_SAMPLER;


    -- testbench labels (rotulos para auxiliar o debug)
    type type_tb_labels is (
    	tb_CPU_INIT, 
    	tb_SEND_CMD_ID_SIZE, 
    	tb_RECV_ID_SIZE, 
    	tb_SEND_CMD_DESCR,
    	tb_RECV_DESCR,
    	tb_SEND_CMD_INT_STATUS,
    	tb_RECV_INT_STATUS,
    	tb_SEND_CS_CODEC,
    	tb_SEND_CMD_CODEC,
    	tb_RECV_DATA_CODEC,
    	tb_SEND_DATA_CODEC,
    	tb_IDLE
    	
    );    
    signal tb_state : type_tb_labels;    
    
    
	-- system clock
	signal i_clk_tb : std_logic := '0';

	signal i_cs_tb       : std_logic := '1';
	signal o_int_tb      : std_logic;
	signal i_rst_tb      : std_logic;
	signal o_led_tb      : std_logic;
	signal i_sclk_tb     : std_logic := '0';
	signal i_mosi_tb     : std_logic;
	signal o_miso_tb     : std_logic;
	signal io_codec_tb   : std_logic;
	signal o_cscodec_tb  : std_logic_vector(BOARD.num_codecs - 1 downto 0);
	signal i_intcodec_tb : std_logic_vector(BOARD.num_codecs - 1 downto 0);
	
	signal serial_in : std_logic := '0';
	signal serial_out : std_logic := '0';
	signal temp2 : std_logic_vector(7 downto 0);
	signal temp : std_logic_vector(7 downto 0);
	signal t_data_out : std_logic_vector(7 downto 0);
	signal t_data_in : std_logic_vector(7 downto 0);
	signal IRQtx : std_logic := '0';
	signal IRQrx : std_logic := '0';
	
	-- sclk sync signals
	signal i_sclk_tb_sync : std_logic := '0';
	signal d_rise_sclk : std_logic;
	signal d_fall_sclk : std_logic;
	-- cs sync signals
	signal i_cs_tb_sync : std_logic := '1';
	signal d_rise_cs : std_logic;
	signal d_fall_cs : std_logic;
	
	signal t_load : std_logic;	
	signal i_reset_aux : std_logic;

	-- testbench clocks period
	constant period_sclk : time := 50 us;
	constant period_clk : time := 1 us;
	constant period_one_byte_spi : time := (period_sclk * 8) + (period_clk * 3);
	signal serial_out_sync : std_logic := '0';
	signal serial_in_sync : std_logic;

	

begin
	UUT : component TOP_RAMAL_UNNITI
		generic map(
			BOARD     => BOARD,
			DESCRITOR => DESCRITOR
		)
		port map(
			i_CLOCK      => i_clk_tb,
			i_RESET      => i_rst_tb,
			o_LED_STATUS => o_led_tb,
			i_SCLK       => i_sclk_tb_sync,
			i_MOSI       => i_mosi_tb,
			i_CS         => i_cs_tb_sync,
			o_MISO       => o_miso_tb,
			o_INTPLACA   => o_int_tb,
			o_CSCODEC    => o_cscodec_tb,
			i_INT_CODEC  => i_intcodec_tb,
			io_CODEC_DIO => io_codec_tb
		);
		
	TB_SCLK_SYNC: component SYNC_SAMPLER
		port map(
			clk    => i_clk_tb,
			rst    => i_reset_aux,
			din    => i_sclk_tb,
			dout   => i_sclk_tb_sync,
			d_rise => d_rise_sclk,
			d_fall => d_fall_sclk
		);
		
	TB_CS_SYNC: component SYNC_SAMPLER
		port map(
			clk    => i_clk_tb,
			rst    => i_reset_aux,
			din    => i_cs_tb,
			dout   => i_cs_tb_sync,
			d_rise => d_rise_cs,
			d_fall => d_fall_cs
		);
		
	TB_MOSI_SYNC : component SYNC_SAMPLER
		port map(
			clk    => i_clk_tb,
			rst    => i_reset_aux,
			din    => serial_out,
			dout   => serial_out_sync
		);
		
	TB_MISO_SYNC : component SYNC_SAMPLER
		port map(
			clk    => i_clk_tb,
			rst    => i_reset_aux,
			din    => serial_in,
			dout   => serial_in_sync
		);

	--# LET THE TESTBENCH BEGINS !!
			-- system clock generator
	i_clk_tb <= not i_clk_tb after period_clk / 2;

	-- SPI clock generator
	i_sclk_tb <= not i_sclk_tb after period_sclk / 2 when i_cs_tb = '0' else '0';
	
	
	-- system reset generator
	reset_proc : process
	begin
		i_rst_tb <= '0';
		wait for 1 us;
		i_rst_tb <= '1';
		wait;
	end process reset_proc;
	
	i_reset_aux <= not i_rst_tb;	
	
	
	Rx_SPI_proc: process (i_clk_tb)
		variable counter : integer := 0;		
	begin
		if(i_clk_tb'event and i_clk_tb = '1') then			
			if ( IRQrx = '0' ) then
									
				if (i_rst_tb = '0') then
					temp <= (others => '0');
				end if;
				
				if (counter = 8) then
					counter := 0;
				end if;
							
				if (d_rise_sclk = '1') then				
						counter := counter +1;
						if (counter <= 8) then
							temp(7 downto 0) <= temp(6 downto 0) & serial_in;							
						else
							counter := 0;							
						end if;	
				end if;
								
			end if;		
			t_data_out <= temp;						
		end if;
		
	end process Rx_SPI_proc;
	

	Tx_SPI_proc: process (i_clk_tb)
		variable counter : integer := 0;
	begin
		if(i_clk_tb'event and i_clk_tb = '1') then
			
			if (IRQtx = '0') then
				if (i_rst_tb = '0') then
					temp2 <= (others => '0');
				end if;
				
				if (t_load = '1') then
					temp2 <= t_data_in;					
				end if;
				
				if(d_fall_cs = '1' or d_fall_sclk = '1') then
					counter := counter +1;
					if (counter <= 8) then
						temp2 <= temp2(6 downto 0) & '0';
						serial_out <= temp2(7);						
					else
						counter := 0;								
					end if;
				end if;
			end if;
		end if;	
	end process Tx_SPI_proc;
	
	-- Tx
	i_mosi_tb <= serial_out_sync;	
	-- Rx
	serial_in <= o_miso_tb;
	
	
	-- DECLARACAO DOS TESTES
	
	-- CASOS DE TESTE -
	-- 1) - envio de comando SPI para recebimento do tamanho do descritor (1 byte)
	--    - recebimento SPI do tamanho do descritor (1 byte)
	
	-- 2) - envio de comando SPI para recebimento do descritor (1 byte)
	--    - recebimento SPI do descritor (vide "DESCRITOR	: type_descritor")
	
	-- 3) - envio de comando SPI para selecionar um dos codecs (1 byte - comando do cpld)
	--    - envio de comando para o codec (1 byte - comando do codec)
	--    - envio de dado de escrita para o codec selecionado (1 byte - comando escrita do codec)
	
	-- 4) - envio de comando SPI para recebimento do status de interrupcao dos codecs (1 byte)
	
	TESTBENCH_1: process
	begin

				-- INIT STATE
				i_intcodec_tb <= "0000";
				
				-- CPU INITIALIZING
	tb_state <= tb_CPU_INIT; ------------ LABEL		
				wait for 50 ms;
		
				--///////////////////////////////////
				-- TEST 1 GET ID SIZE
				--///////////////////////////////////
				
				-- LOAD SPI
				t_load <= '1';
				t_data_in <= x"03";
				IRQtx <= '0';
				wait for 100 us;
				t_load <= '0';
				-- SEND
	tb_state <= tb_SEND_CMD_ID_SIZE; ------------ LABEL
				i_cs_tb <= '0';
				wait for period_one_byte_spi;				
				-- STOP SEND
				i_cs_tb <= '1';
	tb_state <= tb_IDLE;		
				
				-- PAUSE
				wait for 100 us;
				
				-- RECEIVE  <<<<
	tb_state <= tb_RECV_ID_SIZE; ------------ LABEL
				IRQrx <= '0';
				i_cs_tb <= '0';
				wait for period_one_byte_spi;				
				-- STOP
				i_cs_tb <= '1';
	tb_state <= tb_IDLE;
				
				-- PAUSE
				wait for 200 us;
				
				
				--///////////////////////////////////
				-- TEST 2 DESCRITOR READ
				--///////////////////////////////////
				
				-- LOAD SPI command 01
				t_load <= '1';
				t_data_in <= x"01";
				IRQtx <= '0';
				wait for 100 us;
				t_load <= '0';
				-- SEND				
	tb_state <= tb_SEND_CMD_ID_SIZE; ------------ LABEL
				i_cs_tb <= '0';
				wait for period_one_byte_spi;				
				-- STOP SEND
	tb_state <= tb_IDLE; ------------ LABEL
				i_cs_tb <= '1';		
				
				-- PAUSE
				wait for 200 us;
				
				-- RECEIVE			
				for i in 0 to desc_fxo_unniti'length-1 loop
						IRQrx <= '0';
	tb_state <= tb_RECV_DESCR; ------------ LABEL
						i_cs_tb <= '0';
						wait for period_one_byte_spi;				
						-- STOP
	tb_state <= tb_IDLE; ------------ LABEL
						i_cs_tb <= '1';			
						wait for 100 us;
				end loop;
				
				
				-- PAUSE
				wait for 1000 ms;
				
				--///////////////////////////////////
				-- TEST 3 CODEC DATA IN
				--///////////////////////////////////
				
				-- LOAD SPI
				t_load <= '1';
				t_data_in <= x"11";
				IRQtx <= '0';
				wait for 100 us;
				t_load <= '0';
				-- SEND				
	tb_state <= tb_SEND_CS_CODEC; ------------ LABEL
				i_cs_tb <= '0';
				wait for period_one_byte_spi;				
				-- STOP
				i_cs_tb <= '1';		
	tb_state <= tb_IDLE; ------------ LABEL
				
				-- PAUSE
				wait for 100 us;
				
				-- LOAD SPI
				t_load <= '1';
				t_data_in <= x"A5"; -- enviando um comando pois byte anterior x"11"
				IRQtx <= '0';
				wait for 100 us;
				t_load <= '0';
				-- SEND				
	tb_state <= tb_SEND_CS_CODEC; ------------ LABEL
				i_cs_tb <= '0';
				wait for period_one_byte_spi;				
				-- STOP
				i_cs_tb <= '1';	
	tb_state <= tb_IDLE; ------------ LABEL
				wait for 100 ms;		

				--///////////////////////////////////
				-- TEST 4 READ INTERRUPT STATUS REGISTER
				--///////////////////////////////////
				
				-- CODEC INT TEST VECTOR
				i_intcodec_tb <= "0110";
				wait for 100 us; -- aguarda um pouquinho meu quiriduh!
				
				-- LOAD SPI				
				t_load <= '1';
				t_data_in <= x"02"; -- comando para receber o vetor de interrupcao dos codecs
				IRQtx <= '0';
				wait for 100 us;
				t_load <= '0';
				-- SEND
	tb_state <= tb_SEND_CMD_INT_STATUS; ------------ LABEL
				i_cs_tb <= '0';
				wait for period_one_byte_spi;				
				-- STOP
				i_cs_tb <= '1';
	tb_state <= tb_IDLE; ------------ LABEL
	
				wait for 100 us;
				
	tb_state <= tb_RECV_INT_STATUS;	------------ LABEL
				-- RECEIVE
				IRQrx <= '0';
				i_cs_tb <= '0';
				wait for period_one_byte_spi;				
				-- STOP
				i_cs_tb <= '1';
				
				i_intcodec_tb <= "0000"; -- o codec limpa a interrupcao ao termino da comunicacao
				
	tb_state <= tb_IDLE; ------------ LABEL
				wait;
				

	end process TESTBENCH_1;
	
	
end architecture RTL;