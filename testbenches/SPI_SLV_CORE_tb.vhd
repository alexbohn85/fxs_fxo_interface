library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity SPI_SLV_CORE_tb is
	generic(
		bus_width : integer := 8
	);
end entity SPI_SLV_CORE_tb;

architecture RTL of SPI_SLV_CORE_tb is
	component SPI_SLV_CORE
		generic(bus_width : integer := 8);
		port(i_clk      : in  std_logic;
			 i_rst      : in  std_logic;
			 i_sclk     : in  std_logic;
			 i_enable   : in  std_logic;
			 i_load     : in  std_logic;
			 o_irq_spi  : out std_logic;
			 i_si       : in  std_logic;
			 o_so       : out std_logic;
			 i_pi       : in  std_logic_vector(bus_width - 1 downto 0);
			 o_po       : out std_logic_vector(bus_width - 1 downto 0));
	end component SPI_SLV_CORE;

	signal t_clk        : std_logic := '0';
	signal t_sclk       : std_logic := '0';
	signal t_rst        : std_logic := '1';
	signal t_enable     : std_logic := '0';
	signal t_load       : std_logic := '0';
	signal t_pi         : std_logic_vector(7 downto 0);
	signal t_po         : std_logic_vector(7 downto 0);
	signal t_si         : std_logic := '0';
	signal t_so         : std_logic := '0';
	signal t_o_irq_spi : std_logic := '0';	
	
	-- test vector
	signal VECTOR_TEST : std_logic_vector(7 downto 0) := "10011001";

begin

	-- system clock generator
	t_clk <= not t_clk after 1 ns;
	
	-- SPI clock generator
	t_sclk <= not t_sclk after 50 ns when t_enable = '1' and t_o_irq_spi = '0' else '0';
	
	-- chip select disable control
	t_enable <= '1' after 100 ns when t_o_irq_spi = '0' else '0' after 50 ns;
	
	-- system reset generator
	reset_proc : process
	begin
		t_rst <= '1';
		wait for 50 ns;
		t_rst <= '0';
		wait;
	end process reset_proc;

	-- load and send data thru PISO
	load : process(t_enable)
	begin
		if (t_enable'event and t_enable = '1') then
		t_pi <= VECTOR_TEST;
		t_load <= '1';		
		end if;
	end process load;	
	
	-- serial loopback
	t_si <= t_so;
	
	------------------------------
	--:: component instances
	------------------------------

	uut : component SPI_SLV_CORE
		generic map(
			bus_width => bus_width
		)
		port map(
			i_clk      => t_clk,
			i_rst      => t_rst,
			i_sclk     => t_sclk,
			i_enable   => t_enable,
			i_load     => t_load,
			o_irq_spi => t_o_irq_spi,
			i_si       => t_si,
			o_so       => t_so,
			i_pi       => t_pi,
			o_po       => t_po
		);
end architecture RTL;
