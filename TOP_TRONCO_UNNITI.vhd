-- # Module Name: TOP Module 8x Troncos Unniti
-- # Description:

-- Interface de comunicacao SPI / TDM para as placas de FXS e FXO para familia de PABX UnniTI e 
-- Impacta 94/140/220. O barramento de dados eh SPI modo 0 operando como slave. A maquina de estados 
-- principal recebe comandos da CPU(PABX) via SPI e eh responsavel por enviar o descritor da placa, 
-- habilitar o barramento de comunicacao com os CODECs e fornecer status de interrupcao dos codecs. 
-- O barramento TDM passa apenas por buffers para nao sobrecarregar fanout do FPGA da CPU.

-- Device: Altera Max V - 5M160Z
-- IDE: Sigasi Starter Edition
-- 03/10/2015 -> File Created [Alex Bohn]


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- definicoes gerais
use work.FXS_FXO_GLOBALS.all;

entity TOP_TRONCO_UNNITI is
	
	generic(
		BOARD		: type_info 		:= info_fxo_unniti;			-- infomacoes da placa
		DESCRITOR	: type_descritor	:= descritor_fxo_unniti		-- descritor da placa		
	);
	port(
		i_CLOCK      : in    std_logic;
		i_RESET      : in    std_logic;
		o_LED_STATUS : out   std_logic;
		i_SCLK       : in    std_logic;
		i_MOSI       : in    std_logic;
		i_CS         : in    std_logic;
		o_MISO       : out   std_logic;
		o_INTPLACA   : out   std_logic;
		i_DR         : in    std_logic;
		o_DR         : out   std_logic;
		i_DX         : in    std_logic;
		o_DX         : out   std_logic;
		i_F0         : in    std_logic;
		o_F0         : out   std_logic;
		i_C2         : in    std_logic;
		o_C2         : out   std_logic;
		o_CSCODEC    : out   std_logic_vector(BOARD.num_codecs - 1 downto 0);
		i_INT_CODEC  : in    std_logic_vector(BOARD.num_codecs - 1 downto 0);
		io_CODEC_DIO : inout std_logic;
		o_SCLK_CODEC : out   std_logic_vector(BOARD.num_codecs - 1 downto 0)
	);
	
end entity TOP_TRONCO_UNNITI;

architecture RTL of TOP_TRONCO_UNNITI is
		
begin
		
	u0 : entity work.FXS_FXO
		generic map(
			BOARD     => BOARD,
			DESCRITOR => DESCRITOR
		)
		port map(
			i_CLOCK      => i_CLOCK,
			i_RST        => i_RESET,
			o_LED_STATUS => o_LED_STATUS,
			i_SCLK       => i_SCLK,
			i_MOSI       => i_MOSI,
			i_CS         => i_CS,
			o_MISO       => o_MISO,
			o_INTPLACA   => o_INTPLACA,
			i_DR         => i_DR,
			o_DR         => o_DR,
			i_DX         => i_DX,
			o_DX         => o_DX,
			i_F0         => i_F0,
			o_F0         => o_F0,
			i_C2         => i_C2,
			o_C2         => o_C2,
			o_CSCODEC    => o_CSCODEC,
			i_INT_CODEC  => i_INT_CODEC,
			io_CODEC_DIO => io_CODEC_DIO,
			o_SCLK_CODEC => o_SCLK_CODEC
		);
		
end architecture RTL;
